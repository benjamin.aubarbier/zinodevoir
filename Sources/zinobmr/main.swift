import Kitura
import HeliumLogger
import KituraStencil
import Foundation.NSDate

// Lancer les logs
HeliumLogger.use()

// fabriquer le moteur
let router = Router()

router.all(middleware: [BodyParser(), StaticFileServer(path: "./Public")])
router.add(templateEngine: StencilTemplateEngine())

let heure = Calendar.current.component(.hour, from:Date())

var ValeurHeure: String = ""

if heure >= 18 || heure <= 6 {
    ValeurHeure = "Bonsoir"
} else {
    ValeurHeure = "Bonjour"
}

var code : String = ""

func rot(s: String)-> String {
    var key = [Character: Character]()
    let uppercase = Array("ABCDEFGHIJKLMNOPQRSTUVWXYZ")
    let lowercase = Array("abcdefghijklmnopqrstuvwxyz")

    for i in 0 ..< 26 {
        key[uppercase[i]] = uppercase[(i + 13) % 26]
        key[lowercase[i]] = lowercase[(i + 13) % 26]
    }

    return String(s.map { key[$0] ?? $0 })
}

// quoi faire GET /
router.get("/") { request, response, next in
    // request.queryParameters["truc"] 
    try response.render("Home.stencil", context: [
    "ValeurHeure" : ValeurHeure
    ])

    next()
}

router.post("/rot") { request, response, next in
    if let b = request.body?.asURLEncoded {    
        try response.render("encod.stencil", context: ["code": code])
    }
    next()
}


// demarre le serveur
Kitura.addHTTPServer(onPort: 8080, with: router)
Kitura.run()
