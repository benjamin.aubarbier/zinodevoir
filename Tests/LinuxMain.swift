import XCTest

import zinobmrTests

var tests = [XCTestCaseEntry]()
tests += zinobmrTests.allTests()
XCTMain(tests)
